# Mile Zero for WordPress

## Summary

Mile Zero is an opinionated, responsive theme and environment for bootstrapping WordPress development.
This is a _mostly_ blank canvas on which to build your own WordPress Themes.

The methodologies employed are from our experience in developing small to medium WordPress websites
over the past few years. Thanks goes out to all the hard work and research done by the countless individuals
who make the WordPress community a pleasure to be a part of.

This milezero theme in this project is a fork of the [HTML 5 Reset WordPress Theme](https://github.com/murtaugh/HTML5-Reset-WordPress-Theme) by [@murtaugh](http://twitter.com/murtaugh/).

## What Mile Zero gives you:

* The WordPress folder siloed in it's own directory (security FTW!)
* A flexible environment-agnostic `wp-config.php` that eases deployment
* Native support for a `local-config.php` file for your local database credentials
* Normalized CSS styles via [Normalize.css](http://necolas.github.io/normalize.css/)
* Sass and Compass generated stylesheets
* Suzy for Sass/Compass responsive grid system
* Easy to customize theme - remove what you want, make it better!
* [Options Framework](http://wptheming.com/options-framework-plugin/) by [@devinsays](https://twitter.com/devinsays)integration
* Meta tags and other head information populated with the Options Framework in the Admin Dashboard
* [Modernizr.js](http://modernizr.com/)
* Responsive Nav pattern included with [Responsive-Nav](http://responsive-nav.com/)
* IE-specific classes for simple CSS-targeting
* Grunt-based workflow including CSS/JS linting, Sass/Compass compilation, and livereload
* Probabaly more that we're forgetting...

## Getting Started

### WordPress

We like to include the WordPress repository from Github, but feel free to include it from anywhere, as long as it sits in the `/wordpress/` directory.
Having your WordPress installation encapsulated in it's own folder provides at least a couple of benefits. It's harder for malicious bots to find when your
wp-admin is not right off the root directory, and also allows for you to ignore the folder with Git and let the WordPress update process take place
from the WordPress admin.

To get WordPress from Github, execute the following terminal command from the root of your project:

    $ git clone https://github.com/WordPress/WordPress.git wordpress

To ensure you have the latest stable version of WordPress and not a beta, do the following:

    $ cd wordpress && git tag

Next, you'll want to checkout the last tag in the list. At the time of writing, tag _3.7.1_ is the most recent.

    $ git checkout 3.7.1

This puts the /wordpress/ git repo into 'detached HEAD stage'. No worries.

### Grunt

To get started using [Grunt](http://gruntjs.com/) to compile your stylesheets, JavaScripts and the like, run `npm install` from the root
of your WordPress installation. You may need to run `sudo npm install` if you get any errors.

### Bower

All front-end dependancies are managed with [Bower](http://bower.io).

### Notes

Grunt and Bower depend on [Node](http://nodejs.org/) and [npm](http://npmjs.org/).

You can install Bower globally using npm:

    npm install -g bower

You can install Grunt globally using npm:

    npm install -g grunt-cli