<?php
/**
* The base configurations of the WordPress.
*
* This file has the following configurations: MySQL settings, Table Prefix,
* Secret Keys, WordPress Language, and ABSPATH. You can find more information
* by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
* wp-config.php} Codex page. You can get the MySQL settings from your web host.
*
* This file is used by the wp-config.php creation script during the
* installation. You don't have to use the web site, you can just copy this file
* to "wp-config.php" and fill in the values.
*
* @package WordPress
*/

/** Define some environmental constants **/
define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wordpress');
define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);

/** The wp-content directory is out of the core WordPress directory **/
define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');

/** The Default theme **/
define('WP_DEFAULT_THEME', 'milezero');

define('WP_DEBUG', false);

/** If the file local-config.php exists, use the setting there
*  and set a local constant
*/
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
    include( dirname( __FILE__ ) . '/local-config.php' );
    define( 'WP_LOCAL_DEV', true );
    define('WP_DEBUG', true);
} else {
    define('DB_NAME', 'database_name_here');
    define('DB_USER', 'username_here');
    define('DB_PASSWORD', 'password_here');
    define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY',         'insert_key_here');
define('SECURE_AUTH_KEY',  'insert_key_here');
define('LOGGED_IN_KEY',    'insert_key_here');
define('NONCE_KEY',        'insert_key_here');
define('AUTH_SALT',        'insert_key_here');
define('SECURE_AUTH_SALT', 'insert_key_here');
define('LOGGED_IN_SALT',   'insert_key_here');
define('NONCE_SALT',       'insert_key_here');

/**#@-*/


/* WordPress Database Table prefix. */
$table_prefix  = 'mz_';

/*  WordPress Localized Language, defaults to English. */
define('WPLANG', '');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
