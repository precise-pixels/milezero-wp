module.exports = function(grunt) {

// Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
// let us know if our JS is sound
        jshint: {
            options: {
                "bitwise": false,
                "browser": false,
                "curly": false,
                "eqeqeq": false,
                "eqnull": false,
                "es5": false,
                "esnext": false,
                "immed": false,
                "jquery": true,
                "latedef": false,
                "newcap": false,
                "noarg": false,
                "node": false,
                "strict": false,
                "trailing": false,
                "undef": false,
                "globals": {
                    "jQuery": true,
                    "alert": true
                }
            },
            all: [
                'Gruntfile.js',
                'assets/js/global.js'
            ]
        },

// Compile all our Sass files
        compass: {
            dist: {
                options: {
                    config: '.config.rb'
                }
            }
        },

// Concatenate and compress our JS
        uglify: {
            options: {
                beautify: false
            },
            dist: {
                files: {
                    'assets/js/functions.min.js': [
                        'assets/js/functions.js'
                    ]
                }
            }
        },

// Watch our project for changes
        watch: {
            compass: {
                files: [
                    'assets/sass/*.scss',
                    'assets/sass/**/*.scss',
                    'assets/sass/**/**/*.scss'
                ],
                tasks: ['compass'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: [
                    '<%= jshint.all %>'
                ],
                tasks: ['jshint', 'uglify']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'jshint',
        'sass',
        'uglify'
    ]);
};