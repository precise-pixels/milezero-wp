<?php
/**
 * @package WordPress
 * @subpackage 12-Stars-Media-Productions-WordPress-Theme
 * @since twelvestars 1.0
 */
?>
        </div>

		<footer class="footer source-org vcard copyright">
            <div class="container">
    			<small>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></small>
            </div>
		</footer>

	</div>

	<?php wp_footer(); ?>

</body>

</html>
