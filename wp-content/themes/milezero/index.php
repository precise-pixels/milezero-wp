<?php
/**
 * @package WordPress
 * @subpackage 12-Stars-Media-Productions-WordPress-Theme
 * @since twelvestars 1.0
 */
 get_header(); ?>

 	<div class="container">

 		<div class="content__primary" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class('module') ?> id="post-<?php the_ID(); ?>">

					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

					<?php posted_on(); ?>

					<div class="entry">
						<?php the_content(); ?>
					</div>

					<footer class="postmetadata">
						<?php the_tags(__('Tags: ','milezero'), ', ', '<br />'); ?>
						<?php _e('Posted in','milezero'); ?> <?php the_category(', ') ?> |
						<?php comments_popup_link(__('No Comments &#187;','milezero'), __('1 Comment &#187;','milezero'), __('% Comments &#187;','milezero')); ?>
					</footer>

				</article>

			<?php endwhile; ?>

			<?php post_navigation(); ?>

			<?php else : ?>

				<h2><?php _e('Nothing Found','milezero'); ?></h2>

			<?php endif; ?>

		</div>

		<div class="content__secondary" role="complementary">

			<?php get_sidebar(); ?>

		</div>

	</div>

<?php get_footer(); ?>
