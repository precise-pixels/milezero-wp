<?php
/**
 * @package WordPress
 * @subpackage 12-Stars-Media-Productions-WordPress-Theme
 * @since twelvestars 1.0
 */
 get_header(); ?>

 	<div class="container">

 		<div class="content__primary" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class('module') ?> id="post-<?php the_ID(); ?>">

					<h1 class="entry-title"><?php the_title(); ?></h1>

					<div class="entry-content">

						<?php the_content(); ?>

						<?php wp_link_pages(array('before' => __('Pages: '), 'next_or_number' => 'number')); ?>

						<?php the_tags( __('Tags: '), ', ', ''); ?>

						<?php posted_on(); ?>

					</div>

					<?php edit_post_link(__('Edit this entry'),'','.'); ?>

				</article>

			<?php comments_template(); ?>

			<?php endwhile; endif; ?>

			<?php post_navigation(); ?>

		</div>

		<div class="content__secondary" role="complementary">

			<?php get_sidebar(); ?>

		</div>

	</div>

<?php get_footer(); ?>