# .config.rb
require "susy"

sass_dir = "assets/sass"
css_dir = "assets/css"

images_path = "assets/img"

environment = :development

output_stype = :expanded