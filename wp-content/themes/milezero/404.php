<?php
/**
 * @package WordPress
 * @subpackage 12-Stars-Media-Productions-WordPress-Theme
 * @since twelvestars 1.0
 */
 get_header(); ?>

    <div class="container">

        <div class="content__primary" role="main">

            <article class="404">

                <h2><?php _e('Error 404 - Page Not Found','milezero'); ?></h2>

            </article>

        </div>

    </div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>