<?php
/**
 * @package WordPress
 * @subpackage 12-Stars-Media-Productions-WordPress-Theme
 * @since twelvestars 1.0
 */
 get_header(); ?>

 	<div class="container">

 		<div class="content__primary" role="main">

			<?php if (have_posts()) : ?>

				<h2><?php _e('Search Results','milezero'); ?></h2>

				<?php post_navigation(); ?>

				<?php while (have_posts()) : the_post(); ?>

					<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

						<h2><?php the_title(); ?></h2>

						<?php posted_on(); ?>

						<div class="entry">

							<?php the_excerpt(); ?>

						</div>

					</article>

				<?php endwhile; ?>

				<?php post_navigation(); ?>

			<?php else : ?>

				<h2><?php _e('Nothing Found','milezero'); ?></h2>

			<?php endif; ?>

		</div>

		<div class="content__secondary" role="complementary">

			<?php get_sidebar(); ?>

		</div>

	</div>

<?php get_footer(); ?>
