<?php
/**
 * @package WordPress
 * @subpackage 12-Stars-Media-Productions-WordPress-Theme
 * @since twelvestars 1.0
 */
 get_header(); ?>

 	<div class="container">

 		<div class="content__primary content__primary--full">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article class="post module" id="post-<?php the_ID(); ?>">

					<h2><?php the_title(); ?></h2>

					<?php //posted_on(); ?>

					<div class="entry">

						<?php the_content(); ?>

						<?php wp_link_pages(array('before' => __('Pages: '), 'next_or_number' => 'number')); ?>

					</div>

					<?php edit_post_link(__('Edit this entry.'), '<p>', '</p>'); ?>

				</article>

				<?php endwhile; endif; ?>

			<?php //get_sidebar(); ?>

		</div>

	</div>

<?php get_footer(); ?>
